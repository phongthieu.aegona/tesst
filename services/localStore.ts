"use client";
export const localStore= {
    get:()=>{
        let dataJson:any = localStorage?.getItem("USER_INFOR")
        return JSON.parse(dataJson)
    },
    set:(data:any)=>{
        let dataJSON = JSON.stringify(data)
        return localStorage.setItem("USER_INFOR",dataJSON)
    },
    remove:()=>{
        return localStorage.removeItem("USER_INFOR")
    }
}
