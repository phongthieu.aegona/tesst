import { Pacifico } from "next/font/google";

export const pacifico = Pacifico({
  subsets: ["latin"],
  display: "swap",
  weight: "400",
  variable: '--font-pacifico'
});
