"use client";
import { signIn} from "next-auth/react"
import React, { useEffect, useState } from "react";
import "../styles/styles.css";
import { Button, Divider, Form, Input, message } from "antd";
import { localStore } from "@/services/localStore";
import { usePostLoginMutation } from "@/redux/feature/loginApiSlice";
import logo from '../../public/picture/logo-social.png'
import Image from "next/image";
export default function login() {
  const [colSpan, setColSpan] = useState(4);
  useEffect(() => {
    const handleResize = () => {
      const windowWidth = window.innerWidth;
      let newSlidesPerView = 8;

      if (windowWidth >= 768 && windowWidth < 1024) {
        newSlidesPerView = 6;
      } else if (windowWidth >= 1200 && windowWidth < 1440) {
        newSlidesPerView = 6;
      } else if (windowWidth >= 1440) {
        newSlidesPerView = 4;
      }

      setColSpan(newSlidesPerView);
    };

    window.addEventListener("resize", handleResize);
    handleResize(); 

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  const [login] = usePostLoginMutation();
  const onFinish = (values: any) => {
    login(values)
      .unwrap()
      .then((res) => {
        localStore.set(res);
        message.success("Đăng nhập thành công");
      })
      .catch((err) => {
        console.log(err);
        message.error(`Đăng nhập thất bại: ${err.data.content}`);
      });
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div>
      <div className="w-full login-body">
        <Form
          name="basic"
          labelCol={{ span: colSpan }}
          wrapperCol={{ span: 20 }}
          style={{ maxWidth: 600 }}
          initialValues={{ remember: true }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <h1>Login</h1>
          <Form.Item
            label="Username"
            name="taiKhoan"
            
            rules={[{ required: true, message: "Vui lòng nhập tài khoản!" }]}
          >
            <Input placeholder="Nhập tài khoản" />
          </Form.Item>

          <Form.Item
            label="Password"
            name="matKhau"
            rules={[{ required: true, message: "Vui lòng nhập mật khẩu" }]}
          >
            <Input.Password placeholder="Nhập mật khẩu"  />
          </Form.Item>
          <div className="form__forgot-password flex justify-center mb-5  text-white" >
            <a className="form__forgot-password-text" >
              Forgot password
            </a>
          </div>
          <Form.Item wrapperCol={{ offset: 0, span: 24 }}>
            <Button type="primary" htmlType="submit" className="form-submit">
              Sign in
            </Button>
          </Form.Item>
          <Divider className="text-white border-white" >Or LOGIN with ...</Divider>
         
          <Button onClick={() => signIn()} type="primary" htmlType="submit" className="form-submit h-fit">
              <div className="flex flex-row justify-center items-center space-x-5">
                <div>
                  Sign in with social
                </div>
                <Image 
                src={logo}
                width={100}
                height={100}
                alt="Picture of the author"/>
          
              </div>
            </Button>
            <Divider className="text-white border-white" >Don't have an acount</Divider>
            <div className="form__signup flex justify-center text-white " >
            <a className="form__signup-text" >
              Sign up now
            </a>
          </div>
        </Form>
        <p className="text-center text-white text-xs">
          ©2023 AEGONA All rights reserved.
        </p>
      </div>
    </div>
  );
}
