import { localStore } from "@/services/localStore";
import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  userInfo: localStore.get(),
};

const userSlice = createSlice({
  name: "userSlice",
  initialState,
  reducers: {},
});

export const {} = userSlice.actions;

export default userSlice.reducer;
