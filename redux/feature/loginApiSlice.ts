import { localStore } from "@/services/localStore";
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const loginApi = createApi(  {
  reducerPath: "loginApi",
  baseQuery: fetchBaseQuery({
    baseUrl: "https://movienew.cybersoft.edu.vn/",
    prepareHeaders: (header) => {
      header.set(
        "TokenCybersoft",
        `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCA0MCIsIkhldEhhblN0cmluZyI6IjEwLzA5LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY5NDMwNDAwMDAwMCIsIm5iZiI6MTY2NTY4MDQwMCwiZXhwIjoxNjk0NDUxNjAwfQ.sBqNvFEzAEqAZuxinnH_gzedfmLxPTf7WONjIlV-Q7U`
      );
      header.set("Authorization", `Bearer ${localStore.get()?.accessToken}`);
      return header;
    },
  }),

  endpoints: (builder) => ({
    postLogin: builder.mutation({
      query: (loginData) => ({
        url: `api/QuanLyNguoiDung/DangNhap`,
        method: "POST",
        body: loginData,
      }),
    }),
  }),
});


export const { usePostLoginMutation } = loginApi;
