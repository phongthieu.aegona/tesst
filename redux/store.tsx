import { configureStore } from "@reduxjs/toolkit";
import userSlice from "./feature/userSlice";
import { loginApi } from "./feature/loginApiSlice";
import { setupListeners } from "@reduxjs/toolkit/dist/query";

export const store = configureStore({
  reducer: {
    userSlice,
    [loginApi.reducerPath]: loginApi.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(loginApi.middleware),
  devTools: process.env.NODE_ENV !== "production",
});
setupListeners(store.dispatch)
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
